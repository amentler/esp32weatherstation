/*The MIT License (MIT)*/
#include "sensors.h"
#include <EEPROM.h>

#include "bsec.h"
#include <SCD30.h>
#include <Wire.h>
#include <FS.h>
#include <esp_SPIFFS.h>


const uint8_t bsec_config_iaq[] = {
#include "config/generic_33v_300s_4d/bsec_iaq.txt"
};

uint8_t bsecState[BSEC_MAX_STATE_BLOB_SIZE] = {0};
uint16_t stateUpdateCounter = 0;
uint32_t millisOverflowCounter = 0;
uint32_t lastTime = 0;

#define STATE_SAVE_PERIOD  UINT32_C(360 * 60 * 1000) // 360 minutes - 4 times a day

void checkIaqSensorStatus(void);
void startupScdAtI2C() ;
void printLoggingConsoleInput() ;
void setupBME();
void monitorBMEAndGetPressure(Weather *weather);
void printMeasurementsToLoggingConsole(Weather *weather);
void getScdMeasurements(Weather *weather);
void showCarb(float carb);
void showTemp(float temp);
void showHum(float humidity);
bool processTerminalCommand();
void CompensatePressureInSCD30(int pressure);
void loadState();
void updateState();


Bsec iaqSensor;

#define STATE_SAVE_PERIOD UINT32_C(360 * 60 * 1000) // 360 minutes - 4 times a day

int a = 0;

void sensorSetup() {
    Wire.begin();
    startupScdAtI2C();
    
    setupBME();

    Debug(F("Sensor Setup Finished"));
}

bool segmentsFilled = false;
void getCurrentSensorData(Weather *weather) {
    
    monitorBMEAndGetPressure(weather);
    int pressure = weather->bmePressure;

    CompensatePressureInSCD30(pressure);

    getScdMeasurements(weather);

    printMeasurementsToLoggingConsole(weather); 
}

//SETUP
void setupBME() {    
    iaqSensor.begin(BME680_I2C_ADDR_PRIMARY, Wire);
    EEPROM.begin(BSEC_MAX_STATE_BLOB_SIZE + 1); // 1st address for the length
        
    String output;
    output = "\nBSEC library version " + String(iaqSensor.version.major) + "." + 
        String(iaqSensor.version.minor) + "." + String(iaqSensor.version.major_bugfix) + "." + 
        String(iaqSensor.version.minor_bugfix);
    Debug(output);

    checkIaqSensorStatus();

    iaqSensor.setConfig(bsec_config_iaq);
    checkIaqSensorStatus();

    loadState();

    bsec_virtual_sensor_t sensorList[10] = {
        BSEC_OUTPUT_RAW_TEMPERATURE,
        BSEC_OUTPUT_RAW_PRESSURE,
        BSEC_OUTPUT_RAW_HUMIDITY,
        BSEC_OUTPUT_RAW_GAS,
        BSEC_OUTPUT_IAQ,
        BSEC_OUTPUT_STATIC_IAQ,
        BSEC_OUTPUT_CO2_EQUIVALENT,
        BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
        BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
        BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
    };

    iaqSensor.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_ULP);
    checkIaqSensorStatus();

    // Print the header
    

    output = F("Timestamp [ms], raw temperature [°C], pressure [hPa], raw relative humidity [%], gas [Ohm], IAQ, IAQ accuracy, temperature [°C], relative humidity [%], Static IAQ, CO2 equivalent, breath VOC equivalent");
    Debug(output);
}

void startupScdAtI2C() {
    if (scd30.isAvailable()) {        
        Debug(F("SCD available!"));
        scd30.initialize();
        scd30.startPeriodicMeasurment();
    } else {
        Debug(F("SCD NOT available!"));
    }
}

void getScdMeasurements(Weather *weather) {
    float result[3] = {0};
    scd30.getCarbonDioxideConcentration(result);
    weather->scdCarbonDioxide = result[0];
    weather->scdTemperature = result[1];
    weather->scdHumidity = result[2];
}

int _pressure = -1;
bool hasPressureChanged(int pressure) {
    return pressure != _pressure;
}

void CompensatePressureInSCD30(int pressure) {
    if (pressure > -1) {
        pressure = pressure / 100;
        if (hasPressureChanged(pressure)) {
            _pressure = pressure;
            String output = F("Starting Measurement with Pressure Compensation: \n");
            Debug( output + String(pressure) + " mBar");
            scd30.startPeriodicMeasurmentWithPressureCompensation(pressure);
        }
    }
}

void Debug(String output) {
    #ifdef DEBUG
        Serial.println(output);
    #endif
}

void printMeasurementsToLoggingConsole(Weather *weather){      
    Debug(String(F(" ===== READING SCD30 ===== \n")) +
    String(F("Carbon Dioxide Concentration is: ")) + String(weather->scdCarbonDioxide) + " ppm \n" +
    String(F("Temperature = ")) + String(weather->scdTemperature) + " ℃ \n" +
    String(F("Humidity = ")) + String(weather->scdHumidity) + " %");
}


void monitorBMEAndGetPressure(Weather *weather) {
  if (iaqSensor.run()) {
    weather->bmeTemperature = iaqSensor.temperature;
    weather->bmePressure = iaqSensor.pressure;
    weather->bmeHumidity = iaqSensor.humidity;   
    weather->bmeGasResistance = iaqSensor.gasResistance; 
    weather->bmeIaq = iaqSensor.iaq;
    weather->bmeAccuracy = iaqSensor.iaqAccuracy;
    updateState();
    
    Debug(F(" ===== READING BME 680 ====="));
    Debug(String(F("Temperature = ")) + String(weather->bmeTemperature) + " *C");
    Debug(String(F("Pressure = ")) + String(weather->bmePressure  / 100.0) + " hPa");
    Debug(String(F("Humidity = ")) + String(weather->bmeHumidity) + " %");
    Debug(String(F("Gas = ")) + String(weather->bmeGasResistance / 1000.0) + " KOhms");  
    Debug(String(F("Approx. Air Quality = ")) + String(weather->bmeIaq) + " iaq\n");
    Debug(String(F("Approx. Accuracy = ")) + String(weather->bmeAccuracy) );
  } else {
    checkIaqSensorStatus();
  } 
}

// Helper function definitions
void checkIaqSensorStatus(void)
{
    String output;
  if (iaqSensor.status != BSEC_OK) {
    if (iaqSensor.status < BSEC_OK) {
      output = String(F("BSEC error code : ")) + String(iaqSensor.status);
      Debug(output);
    } else {
      output = String(F("BSEC warning code : ")) + String(iaqSensor.status);
      Debug(output);
    }
  }

  if (iaqSensor.bme680Status != BME680_OK) {
    if (iaqSensor.bme680Status < BME680_OK) {
      output = String(F("BME680 error code : ")) + String(iaqSensor.bme680Status);
      Debug(output);
    } else {
      output = String(F("BME680 warning code : ")) + String(iaqSensor.bme680Status);
      Debug(output);
    }
  }
}


void loadState(void)
{
  if (EEPROM.read(0) == BSEC_MAX_STATE_BLOB_SIZE) {
    // Existing state in EEPROM
    Serial.println("Reading state from EEPROM");

    for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE; i++) {
      bsecState[i] = EEPROM.read(i + 1);
      Serial.println(bsecState[i], HEX);
    }

    iaqSensor.setState(bsecState);
    checkIaqSensorStatus();
  } else {
    // Erase the EEPROM with zeroes
    Serial.println("Erasing EEPROM");

    for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE + 1; i++)
      EEPROM.write(i, 0);

    EEPROM.commit();
  }
}

void updateState(void)
{
  bool update = false;
  if (stateUpdateCounter == 0) {
    /* Set a trigger to save the state. Here, the state is saved every STATE_SAVE_PERIOD 
    with the first state being saved once the algorithm achieves full calibration, i.e. iaqAccuracy = 3 */
    if (iaqSensor.iaqAccuracy >= 3) {
      update = true;
      stateUpdateCounter++;
    }
  } else {
    /* Update every STATE_SAVE_PERIOD milliseconds */
    if ((stateUpdateCounter * STATE_SAVE_PERIOD) < millis()) {
      update = true;
      stateUpdateCounter++;
    }
  }

  if (update) {
    iaqSensor.getState(bsecState);
    checkIaqSensorStatus();

    Serial.println("Writing state to EEPROM");

    for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE ; i++) {
      EEPROM.write(i + 1, bsecState[i]);
      Serial.println(bsecState[i], HEX);
    }

    EEPROM.write(0, BSEC_MAX_STATE_BLOB_SIZE);
    EEPROM.commit();
  }
}

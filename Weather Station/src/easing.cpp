#include "easing.h"
#include <Arduino.h>

int boundary(int value, int minValue, int maxValue);

double normalize(int value, int minValue, int maxValue);

double ease(double x);

int calculateColorIntensity(int sensorValue,  int minValue, int minColor, int maxValue, int maxColor);

int boundary(int value, int minValue, int maxValue) {
  return max(minValue, min(value, maxValue));;
}

double normalize(int value, int minValue, int maxValue) {
  return (value - minValue) / (double)(maxValue - minValue);
}

double ease(double x) {
  return (sin((x*PI) - (PI/2.) ) + 1. )/ 2.;
}

int calculateColorIntensity(int sensorValue,  int minValue, int minColor, int maxValue, int maxColor) {
    sensorValue = boundary(sensorValue, minValue, maxValue); //sensor value now is between the limits
    //Serial.println("Bounded SensorValue:" + String(sensorValue));
    double normalizedSensorvalue = normalize(sensorValue, minValue, maxValue); //its normalized between 0 and 1 so we can use easing
    //Serial.println("NormalizedSensorValue:" + String(normalizedSensorvalue));
    double easedValue = ease(normalizedSensorvalue); // its now projected and slightly corrected if it isnt far of min or max
    //Serial.println("EasedValue:" + String(easedValue));
    int easedColorOffset = (maxColor - minColor) * easedValue; //its transformed into a color offset from min
    //Serial.println("easedOffsett:" + String(easedColorOffset));
    return minColor + easedColorOffset;
}

/*projection from a sensor value to a color value.
*/
int calculateColorIntensity(int sensorValue, int normalValue, int normalColor, int minValue, int minColor, int maxValue, int maxColor) {
  if (sensorValue <= normalValue) {
    return calculateColorIntensity(sensorValue, minValue, minColor, normalValue, normalColor);
  } else {
    return calculateColorIntensity(sensorValue, normalValue, normalColor, maxValue, maxColor);
  }
}
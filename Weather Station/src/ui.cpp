#include "ui.h"
#include <Arduino.h>
#include <WiFi.h>
#include <FS.h>
#include <esp_SPIFFS.h>
#include <SPI.h>

#include <Adafruit_GFX.h>
#include <Adafruit_STMPE610.h>
#include "TouchControllerWS.h"

#include <JsonListener.h>
#include <OpenWeatherMapForecast.h>
#include <ILI9341_SPI.h>


#include "ArialRounded.h"
#include "moonphases.h"
#include "weathericons.h"

// defines the colors usable in the paletted 16 color frame buffer
extern uint16_t palette[] = {
                      ILI9341_BLACK, // 0
                      ILI9341_WHITE, // 1
                      ILI9341_YELLOW, // 2
                      0x7E3C
                     }; //3

String getTime(time_t *timestamp) {
  struct tm *timeInfo = gmtime(timestamp);
  
  char buf[6];
  sprintf(buf, "%02d:%02d", timeInfo->tm_hour, timeInfo->tm_min);
  return String(buf);
}

// draw moonphase and sunrise/set and moonrise/set
void drawAstronomy(MiniGrafx gfx, 
String moonAgeImage, uint8_t moonAge, time_t dstOffset,
OpenWeatherMapCurrentData currentWeather,
Astronomy::MoonData moonData, String moonPhase) {

  const int x = 5;
  const int y = 250;

  gfx.setFont(MoonPhases_Regular_36);
  gfx.setColor(MINI_WHITE);
  gfx.setTextAlignment(TEXT_ALIGN_CENTER);
  gfx.drawString(x+115, y+25, moonAgeImage);

  gfx.setFont(ArialRoundedMTBold_14);
  gfx.setColor(MINI_YELLOW);
  gfx.drawString(x+115, y, moonPhase);
  
  gfx.setTextAlignment(TEXT_ALIGN_LEFT);
  gfx.drawString(x, y, "Sun");
  gfx.setColor(MINI_WHITE);
  time_t time = currentWeather.sunrise + dstOffset;
  gfx.drawString(x, y+26, "Rise:");
  gfx.drawString(x+40,  y+26, getTime(&time));
  time = currentWeather.sunset + dstOffset;
  gfx.drawString(x, y+41, "Set:");
  gfx.drawString(x+40, y+41, getTime(&time));

  gfx.setTextAlignment(TEXT_ALIGN_RIGHT);
  gfx.setColor(MINI_YELLOW);
  gfx.drawString(x+230, y, "Moon");
  gfx.setColor(MINI_WHITE);
  gfx.drawString(x+230,  y+26, String(moonAge) + "d");
  gfx.drawString(x+230,  y+41, String(moonData.illumination * 100, 0) + "%");
  gfx.drawString(x+195,  y+26, "Age:");
  gfx.drawString(x+195, y+41, "Illum:");
}

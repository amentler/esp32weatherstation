#include "BlingBling.h"

#include <NeoPixelBus.h>
#include "Animations.h"

//this file controls
//the hardware
//the initialisation and order of the animations
//the animation speed

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN 26

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 24

#define ANIMATIONTIME 50

typedef NeoRbgFeature MyPixelColorFeature;
NeoPixelBus<MyPixelColorFeature, Neo800KbpsMethod> strip(LED_COUNT, LED_PIN);


void transition();
bool hasDataChanged(unsigned long time, Weather weather);

void setupBling() {
  strip.Begin();           
  strip.ClearTo(black);
  strip.Show();
}

bool isActive = false;
unsigned long updateTimer = 0;
uint16_t frame = 0;

unsigned long cachedTime;
float cachedHumidity;
float cachedTemperature;
float cachedCo2;

StateFunction currentStateFunc;

BlingState state = NOTHING;

void StartBling(unsigned long time, Weather weather){
  Serial.println(F("Start Bling"));  
  strip.ClearTo(black);
  strip.Show();            
  updateTimer = millis();
  frame = 0;
  isActive = true;
  state = NOTHING;
  transition();
}

void EndBling(){    
  Serial.println(F("End Bling"));
  strip.ClearTo(black);
  strip.Show();           
  isActive = false;
}

int animationRepetitions = 0;
void UpdateBling(unsigned long time, Weather weather) {
    if ((millis() - updateTimer) > ANIMATIONTIME) { 
      updateTimer = millis();

      if (hasDataChanged(time, weather)) {
        updateAnimationColors(time, weather);
      }

      bool finished = currentStateFunc(time, weather, frame);
      for (int counter = 0; counter < 24; counter ++) {
        strip.SetPixelColor(counter, ringColors[counter]);
      }
      strip.Show();
      
      frame++;

      if (finished ) {
        animationRepetitions ++;
      }

      if (animationRepetitions == 3) {
        transition();
      }                        
    }
}

bool hasDataChanged(unsigned long time, Weather weather) {
  bool result = false;

  if (abs(time-cachedTime) > 60000 ) {
    cachedTime = time;
    result = true;
  }
  
  if (cachedHumidity != weather.scdHumidity) {
    cachedTime = cachedHumidity;
    result = true;
  }
  
  if (cachedTemperature != weather.scdTemperature) {
    cachedTime = cachedTemperature;
    result = true;
  }
  
  if (cachedCo2 != weather.scdCarbonDioxide) {
    cachedTime = cachedCo2;
    result = true;
  }

  return result;
}

bool isBlingActive(){
    return isActive;
}

void transition() {
  frame = 0;
  animationRepetitions = 0;
  strip.ClearTo(black);

  switch (state) {
    case NOTHING:
      state = PULSEUP;
      currentStateFunc = pulseUp;
    break;

    case ANIMBUILD:
      state = ANIMKITT;
      currentStateFunc = animKitt;
    break;

    case ANIMKITT:
      state = BUILDUP;
      currentStateFunc = buildUp;
    break;

    case BUILDUP:
      state = ANIMBUILD;
      currentStateFunc = animBuild;
    break;
  }

  strip.Show();
}




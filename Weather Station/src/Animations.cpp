#include "Animations.h"
#include <NeoPixelBus.h>
#include <Arduino.h>
#include "sensors.h"
#include "easing.h"

//this controls
//the general look and steps of the animations
//the coloring
//the brightness

#define colorSaturation 200

RgbColor black(0);
RgbColor white(colorSaturation);
RgbColor lightGrey(colorSaturation/3);
RgbColor grey(colorSaturation/9);
RgbColor darkGrey(colorSaturation/27);
RgbColor ringColors[24] =  {};

void Clear();
void SetPixelColor(int, RgbColor);   

RgbColor colors[24] =  {}; //colors
RgbColor intensity[24] =  {}; //white/gray level

void Clear() {
    for (int counter = 0; counter < 24; counter ++) {
        SetPixelColor(counter, black);
    }
}

void SetPixelColor(int index, RgbColor color) {
    colors[index] = color;
    ringColors[index] = color;
}

bool animBuild(unsigned long time, Weather weather, int frame) {
  int led = frame % 47 ;

  Serial.println("I: " + String(led));
  if (led == 0) {    
    Serial.println("Reset " + String(led));          
    Clear();
  }

  if (led > 23) {
    SetPixelColor(led-24, black);      
  } else {
    SetPixelColor(led, grey);         
  }

  return frame>0 && led+1 == 47;
}

int getLedByFrame(int frame) {
  return 23 - abs((max(0,frame) % 47) - 23);
}

bool animKitt(unsigned long time, Weather weather, int frame) {
  int led = getLedByFrame(frame);
  int led2 = getLedByFrame(frame - 1);
  int led3 = getLedByFrame(frame - 2);
  int led4 = getLedByFrame(frame - 3);
  int deactivateLed = getLedByFrame(frame - 4);

  SetPixelColor(deactivateLed, black); 
  SetPixelColor(led4, darkGrey); 
  SetPixelColor(led3, grey); 
  SetPixelColor(led2, lightGrey); 
  SetPixelColor(led, white); 

  return frame > 0 && (frame % 47) + 1 == 47;
}

bool buildUp(unsigned long time, Weather weather, int frame) {
  const int minColor = 1;
  const int maxColor = 30;
  const int normalColor = 15;

  int greenish = calculateColorIntensity(weather.scdCarbonDioxide, 500, normalColor, 410, minColor, 1000, maxColor);
  RgbColor greenishWhite(greenish, maxColor - greenish, maxColor - greenish);
  int reddish = calculateColorIntensity((weather.scdTemperature+weather.bmeTemperature)/2, 20, normalColor, 16, minColor, 25, maxColor);
  RgbColor reddishWhite(maxColor - reddish, maxColor - reddish, reddish);
  int blueish = calculateColorIntensity((weather.scdHumidity+weather.bmeHumidity)/2, 45, normalColor, 30, minColor, 60, maxColor);
  RgbColor blueishWhite(maxColor - blueish, blueish, maxColor - blueish);

  Serial.println("Red: " + String(reddish) + "Blue: " + String(blueish) + "Green: " + String(greenish));

  int led = frame % 47 ;

  Serial.println("I: " + String(led));
  if (led == 0) {    
    Serial.println("Reset " + String(led));           
    Clear();
  }

  if (led > 23) {
    SetPixelColor(led-24, black);      
  } else {
    if (led < 8) {
      RgbColor co2Color = greenishWhite;
      if (led < 3)
        co2Color = RgbColor::LinearBlend(blueishWhite, greenishWhite, (led+4)/7.);
      else if (led > 4)
        co2Color = RgbColor::LinearBlend(greenishWhite, reddishWhite, (led-4)/7.);
      SetPixelColor(led, co2Color);   
    }   
    else if (led < 16) {
      RgbColor tempColor = reddishWhite;
      if (led < 11)
        tempColor = RgbColor::LinearBlend(greenishWhite, reddishWhite, (led-4)/7.);
      else if (led > 12)
        tempColor = RgbColor::LinearBlend(reddishWhite, blueishWhite, (led-11)/7.);
      SetPixelColor(led, tempColor); 
    }
    else {
      RgbColor humColor = blueishWhite;
      if (led < 19)
        humColor = RgbColor::LinearBlend(reddishWhite, blueishWhite, (led-11)/7.);
      else if (led>20)
        humColor = RgbColor::LinearBlend(blueishWhite, greenishWhite, (led-20)/7.);
      SetPixelColor(led, humColor); 
    }    
  }

  return frame>0 && led+1 == 47;  
}

void updateAnimationColors(unsigned long time, Weather weather) {
  const int minColor = 1;
  const int maxColor = colorSaturation;
  const int normalColor = colorSaturation/2;
  int greenish = calculateColorIntensity(weather.scdCarbonDioxide, 500, normalColor, 410, minColor, 1000, maxColor);
  RgbColor greenishWhite(greenish, maxColor - greenish, maxColor - greenish);
  int reddish = calculateColorIntensity((weather.scdTemperature+weather.bmeTemperature)/2, 20, normalColor, 16, minColor, 25, maxColor);
  RgbColor reddishWhite(maxColor - reddish, maxColor - reddish, reddish);
  int blueish = calculateColorIntensity((weather.scdHumidity+weather.bmeHumidity)/2, 45, normalColor, 30, minColor, 60, maxColor);
  RgbColor blueishWhite(maxColor - blueish, blueish, maxColor - blueish);

  Serial.println("Red: " + String(reddish) + "Blue: " + String(blueish) + "Green: " + String(greenish));

  RgbColor co2Color = greenishWhite;
  RgbColor tempColor = reddishWhite;
  RgbColor humColor = blueishWhite;


  for (int led = 0; led < 24; led ++) {  
    float progress = (float)(((led + 4)%8))/ 8.;

    Serial.println("Progress: " + String(progress));
    if (led >= 20 || led < 4) {
        colors[led] = RgbColor::LinearBlend(humColor, co2Color, progress);
    } else if (led >= 12) {
        colors[led] = RgbColor::LinearBlend(tempColor, humColor, progress);
    } else if (led >= 4) {
        colors[led] = RgbColor::LinearBlend(co2Color, tempColor, progress);
    }
  }
}

int triangleFunction(int counter, int ceil) {
  const int dimTime = ceil; //the number of discrete steps it takes to get up or down to min/max
  int counter2 = counter % dimTime; //make counter a repeating counter from 0 to dimTime. f(counter) -> sabertooth
  counter2 = abs(counter2 - (dimTime/2)); //make it a repeating triangle
  counter2 = (dimTime/2) - counter2; //mirroring so counter2 starts at 0 again
  return counter2;
}

bool pulseUp(unsigned long time, Weather weather, int counter) {
  int pulseSteps = 30;
  int counter2 = triangleFunction(counter, pulseSteps); 

  for (int i = 0; i < 24; i++)
  {
    ringColors[i] = colors[i].Dim((colorSaturation/pulseSteps)*((counter2+5)/1.2));
  }

  return false;
}

bool kittClock(unsigned long time, Weather weather, int counter) {
  return false;
}
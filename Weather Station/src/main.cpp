/*****************************
 * see license @ bottom for readability
 * Important: see settings.h to configure your settings!!!
 * ***************************/
#include "settings.h"

#include <Arduino.h>
#include <WiFi.h>
#include <FS.h>
#include <esp_SPIFFS.h>
#include <SPI.h>
#include "sensors.h"
#include "BlingBling.h"
#include "ui.h"

#include <Adafruit_GFX.h>
#include <Adafruit_STMPE610.h>
#include "TouchControllerWS.h"

#include <JsonListener.h>
#include <OpenWeatherMapCurrent.h>
#include <OpenWeatherMapForecast.h>
#include <Astronomy.h>
#include <MiniGrafx.h>
#include <Carousel.h>
#include <ILI9341_SPI.h>


#include "ArialRounded.h"
#include "moonphases.h"
#include "weathericons.h"

#define MAX_FORECASTS 12

// Limited to 4 colors due to memory constraints
int BITS_PER_PIXEL = 2; // 2^2 =  4 colors

ILI9341_SPI tft = ILI9341_SPI(TFT_CS, TFT_DC);
MiniGrafx gfx = MiniGrafx(&tft, BITS_PER_PIXEL, palette);
Carousel carousel(&gfx, 0, 0, 240, 100);

Adafruit_STMPE610 ts(TOUCH_CS);
TouchControllerWS touchController(&ts);

void calibrationCallback(int16_t x, int16_t y);
CalibrationCallback calibration = &calibrationCallback;
 

OpenWeatherMapCurrentData currentWeather;
Weather weather;
OpenWeatherMapForecastData forecasts[MAX_FORECASTS];
simpleDSTadjust dstAdjusted(StartRule, EndRule);
Astronomy::MoonData moonData;

void updateData();
void drawProgress(uint8_t percentage, String text);
void drawTime();
void drawWifiQuality();
void drawCurrentWeather();
void drawForecast();
void drawForecastDetail(uint16_t x, uint16_t y, uint8_t dayIndex);
void drawInsideData();
void drawCurrentWeatherDetail();
void drawLabelValue(uint8_t line, String label, String value);
void drawForecastTable(uint8_t start);
void drawAbout();
void drawSeparator(uint16_t y);
const char* getMeteoconIconFromProgmem(String iconText);
const char* getMiniMeteoconIconFromProgmem(String iconText);
void drawForecast1(MiniGrafx *display, CarouselState* state, int16_t x, int16_t y);
void drawForecast2(MiniGrafx *display, CarouselState* state, int16_t x, int16_t y);
void drawForecast3(MiniGrafx *display, CarouselState* state, int16_t x, int16_t y);

void enableDisplayLED();
void disableDisplayLED();

FrameCallback frames[] = { drawForecast1, drawForecast2, drawForecast3 };
int frameCount = 3;

// how many different screens do we have?
int screenCount = 5;
long lastDownloadUpdate = millis();

String moonAgeImage = "";
uint8_t moonAge = 0;
uint16_t screen = 0;
long lastTouchTimer;
time_t dstOffset = 0;


void initScreen();
void initGfx();

void connectWifi();
void initFileSystem();
void calibrateTouchScreen();

bool isDisplayEnabled = true;

void printSpaceInfo() {  
  Serial.println("\n=====");
  Serial.println("Free Heap" + String(ESP.getFreeHeap() / 1024 )+"kb");
  Serial.println("Sketch Space" + String(ESP.getFreeSketchSpace() / 1024 )+"kb");
  Serial.println("SketchSize" + String(ESP.getSketchSize() / 1024 )+"kb");
  Serial.println("FlashSize" + String(ESP.getFlashChipSize() / 1024 )+"kb");
  Serial.println("=====");
}

void setup() {
  Serial.begin(921600);

  while (!Serial);
  
  Serial.println("BOOTING!");
  printSpaceInfo();
  Serial.println(F("Init GFX"));
  initGfx(); 

  printSpaceInfo();
  Serial.println(F("Init Screen"));
  initScreen();

  printSpaceInfo();
  Serial.println(F("Sensor Setup"));
  sensorSetup();

  printSpaceInfo();
  Serial.println(F("NeoPixel Setup"));
  setupBling();

  // The LED pin needs to set HIGH
  // Use this pin to save energy
  // Turn on the background LED
  //disable for sensors to work
  printSpaceInfo();
  Serial.println(F("Init Display LED"));
  Serial.println(TFT_LED);
  pinMode(TFT_LED, OUTPUT);
  enableDisplayLED();
  
  delay(100); 

  printSpaceInfo();
  Serial.println(F("Init File System"));
  initFileSystem();

  printSpaceInfo();
  Serial.println(F("Init Touch Calibration"));
  calibrateTouchScreen();

  printSpaceInfo();
  Serial.println(F("Init Carousel"));
  carousel.setFrames(frames, frameCount);
  carousel.disableAutoTransition();
  carousel.disableAllIndicators();

  printSpaceInfo();
  Serial.println(F("Init WiFi"));
  // update the weather information
  connectWifi();
  
  printSpaceInfo();
  Serial.println(F("Update Weather Data"));
  updateData();

  printSpaceInfo();
  Serial.println(F("Init Sensor Data"));
  getCurrentSensorData(&weather);

  lastTouchTimer = millis();
}

void initGfx() {
  gfx.init();
  Serial.println(F("GFX Initialized"));
  Serial.println(F("Setting Orientation"));
  tft.setRotation(2);
  gfx.fillBuffer(MINI_BLACK);
  gfx.commit();
}

void initFileSystem() {  
  Serial.println(F("Mounting file system..."));
  bool isFSMounted = SPIFFS.begin();
  if (!isFSMounted) {
    Serial.println(F("Formatting file system..."));
    drawProgress(50,F("Formatting file system"));
    SPIFFS.format();
  }
  drawProgress(100,F("Formatting done"));
}

void initScreen() {  
  Serial.println(F("Initializing touch screen..."));
  if (!ts.begin()) {
    Serial.println(F("Couldn't start touchscreen controller"));
    while (1);
  }
}

void calibrateTouchScreen() {
  //SPIFFS.remove("/calibration.txt");
  boolean isCalibrationAvailable = touchController.loadCalibration();
  if (!isCalibrationAvailable) {
    Serial.println(F("Calibration not available"));
    touchController.startCalibration(&calibration);
    while (!touchController.isCalibrationFinished()) {
      gfx.fillBuffer(0);
      gfx.setColor(MINI_YELLOW);
      gfx.setTextAlignment(TEXT_ALIGN_CENTER);
      gfx.drawString(140, 240, F("Please calibrate\ntouch screen by\ntouch point"));
      touchController.continueCalibration();
      gfx.commit();
      yield();
    }
    touchController.saveCalibration();
  } else {
    Serial.println(F("Touchscreen calibrated"));
  }
}

void connectWifi() {
  if (WiFi.status() == WL_CONNECTED) return;
  //Manual Wifi
  Serial.print(F("Connecting to WiFi "));
  Serial.print(WIFI_SSID);
  Serial.print("/");
  Serial.println(WIFI_PASS);
  WiFi.mode(WIFI_STA);
  WiFi.setHostname(WIFI_HOSTNAME);
  WiFi.begin(WIFI_SSID,WIFI_PASS);
  int i = 0;
  int cancelCount = 0;
  while (WiFi.status() != WL_CONNECTED && cancelCount < 5) {
    delay(500);
    if (i>80)  { i=0; cancelCount ++;}
    drawProgress(i,"Connecting to WiFi '" + String(WIFI_SSID) + "'");
    i+=10;
    Serial.print(".");
  }
  drawProgress(100,"Connected to WiFi '" + String(WIFI_SSID) + "'");
  Serial.print(F("Connected...\n"));
}

bool reactivateDisplayBeforeScreenSwitch();
bool isNewTouch();
bool isTouchActive();
void endTouch();
void beginTouch();
void onTouch(TS_Point p);
void onTouchHold(TS_Point p);

int sensorTimer = millis();
void HandleTouch();

void loop() {
  if ((millis() - sensorTimer) > SENSOR_UPDATE_TIME ) {
    Serial.println(F("Getting Sensor Data"));
    sensorTimer = millis();
    getCurrentSensorData(&weather);
  }

  HandleTouch();

  // Check if we should update weather information
  if (millis() - lastDownloadUpdate > 1000 * UPDATE_INTERVAL_SECS) {
      updateData();
      lastDownloadUpdate = millis();
  }

  if (isDisplayEnabled) {
    gfx.fillBuffer(MINI_BLACK);
    if (SLEEP_INTERVAL_SECS && ((millis() - lastTouchTimer) >= (SLEEP_INTERVAL_SECS * 1000))){ // after 2 minutes go to sleep
      Serial.println(F("Going to Sleep")); 
      gfx.commit();
      disableDisplayLED();
      StartBling(millis(), weather);
    } else {
      if (screen == 0) {
        drawTime();    
        drawWifiQuality();
        carousel.update();
        drawCurrentWeather();
        drawAstronomy(gfx, moonAgeImage, moonAge,  dstOffset,
                        currentWeather,
                        moonData, MOON_PHASES[moonData.phase]);
        drawInsideData();
      } else if (screen == 1) {
        drawCurrentWeatherDetail();
      } else if (screen == 2) {
        drawForecastTable(0);
      } else if (screen == 3) {
        drawForecastTable(4);
      } else if (screen == 4) {
        drawAbout();
      }
      gfx.commit();
    }
  } else {    
      UpdateBling(millis(), weather);
  }
}

bool reactivateDisplayBeforeScreenSwitch() {
  return !isDisplayEnabled; //when display is off then reactivate before switch
}

unsigned long touchLengthTimer = 0;
void onTouch(TS_Point p) {  
  Serial.print(F("Touchscreen touch: ")); 
  Serial.print(p.x); Serial.print(", "); 
  Serial.print(p.y);Serial.print(", "); 
  Serial.println(p.z);
  
  if (p.y < 80) {
    IS_STYLE_12HR = !IS_STYLE_12HR;
  } else {
    if (!reactivateDisplayBeforeScreenSwitch()) {
      screen = (screen + 1) % screenCount;
      gfx.fillBuffer(MINI_BLACK);
    }
  }
  
  lastTouchTimer = millis();
}

void onTouchHold(TS_Point p) { 
  Serial.println(F("onTouchHold"));
  if ((millis() - touchLengthTimer) > 2000) {
    enableDisplayLED();
    EndBling();
  }
  lastTouchTimer = millis();
}

void enableDisplayLED() {
  Serial.println(F("enabling Display LED"));
    isDisplayEnabled = true;
    digitalWrite(TFT_LED, HIGH); //huzzah onboard led anschalten
}

void disableDisplayLED() {  
  Serial.println(F("disabling Display LED"));
    isDisplayEnabled = false;
    digitalWrite(TFT_LED, LOW); //huzzah onboard led abschalten
}

// Update the internet based information and update screen
void updateData() {  
  gfx.fillBuffer(MINI_BLACK);
  gfx.setFont(ArialRoundedMTBold_14);

  Serial.println(F("Updating time..."));
  drawProgress(10, F("Updating time..."));
  configTime(UTC_OFFSET * 3600, 0, NTP_SERVERS);
  while(!time(nullptr)) {
    Serial.print("#");
    delay(100);
  }
  // calculate for time calculation how much the dst class adds.
  dstOffset = UTC_OFFSET * 3600 + dstAdjusted.time(nullptr) - time(nullptr);
  Serial.printf("Time difference for DST: %d", dstOffset);

  printSpaceInfo();
  Serial.println(F("Updating conditions..."));
  drawProgress(50, F("Updating conditions..."));
  OpenWeatherMapCurrent *currentWeatherClient = new OpenWeatherMapCurrent();
  currentWeatherClient->setMetric(IS_METRIC);
  currentWeatherClient->setLanguage(OPEN_WEATHER_MAP_LANGUAGE);
  currentWeatherClient->updateCurrent(&currentWeather, OPEN_WEATHER_MAP_APP_ID, OPEN_WEATHER_MAP_LOCATION);
  delete currentWeatherClient;
  currentWeatherClient = nullptr;

  printSpaceInfo();
  Serial.println(F("Updating forecasts..."));
  drawProgress(70, F("Updating forecasts..."));
  OpenWeatherMapForecast *forecastClient = new OpenWeatherMapForecast();
  forecastClient->setMetric(IS_METRIC);
  forecastClient->setLanguage(OPEN_WEATHER_MAP_LANGUAGE);
  uint8_t allowedHours[] = {12, 0};
  forecastClient->setAllowedHours(allowedHours, sizeof(allowedHours));
  
  forecastClient->updateForecasts(forecasts, OPEN_WEATHER_MAP_APP_ID, OPEN_WEATHER_MAP_LOCATION, MAX_FORECASTS);
  delete forecastClient;
  forecastClient = nullptr;

  drawProgress(80, "Updating astronomy...");
  Astronomy *astronomy = new Astronomy();
  moonData = astronomy->calculateMoonData(time(nullptr));
  float lunarMonth = 29.53;
  moonAge = moonData.phase <= 4 ? lunarMonth * moonData.illumination / 2 : lunarMonth - moonData.illumination * lunarMonth / 2;
  moonAgeImage = String((char) (65 + ((uint8_t) ((26 * moonAge / 30) % 26))));
  delete astronomy;
  astronomy = nullptr;
  delay(1000);
}

// Progress bar helper
void drawProgress(uint8_t percentage, String text) {
  gfx.fillBuffer(MINI_BLACK);
  gfx.setFont(ArialRoundedMTBold_14);
  gfx.setTextAlignment(TEXT_ALIGN_CENTER);
  gfx.setColor(MINI_WHITE);
  gfx.drawString(160, 90, F("Progress"));
  gfx.setColor(MINI_YELLOW);

  gfx.drawString(160, 146, text);
  gfx.setColor(MINI_WHITE);
  gfx.drawRect(10, 168, SCREEN_WIDTH - 20, 15);
  gfx.setColor(MINI_BLUE);
  gfx.fillRect(12, 170, 299 * percentage / 100, 11);

  gfx.commit();
}

// draws the clock
void drawTime() {

  char time_str[11];
  char *dstAbbrev;
  time_t now = dstAdjusted.time(&dstAbbrev);
  struct tm * timeinfo = localtime (&now);

  gfx.setTextAlignment(TEXT_ALIGN_CENTER);
  gfx.setFont(ArialRoundedMTBold_14);
  gfx.setColor(MINI_WHITE);
  String date = ctime(&now);
  date = date.substring(0,11) + String(1900 + timeinfo->tm_year);
  gfx.drawString(120, 6, date);

  gfx.setFont(ArialRoundedMTBold_36);

  if (IS_STYLE_12HR) {
    int hour = (timeinfo->tm_hour+11)%12+1;  // take care of noon and midnight
    sprintf(time_str, "%2d:%02d:%02d\n",hour, timeinfo->tm_min, timeinfo->tm_sec);
    gfx.drawString(120, 20, time_str);
  } else {
    sprintf(time_str, "%02d:%02d:%02d\n",timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
    gfx.drawString(120, 20, time_str);
  }

  gfx.setTextAlignment(TEXT_ALIGN_LEFT);
  gfx.setFont(ArialMT_Plain_10);
  gfx.setColor(MINI_BLUE);
  if (IS_STYLE_12HR) {
    sprintf(time_str, "%s\n%s", dstAbbrev, timeinfo->tm_hour>=12?"PM":"AM");
    gfx.drawString(195, 27, time_str);
  } else {
    sprintf(time_str, "%s", dstAbbrev);
    gfx.drawString(195, 27, time_str);  // Known bug: Cuts off 4th character of timezone abbreviation
  }
}

// draws current weather information
void drawCurrentWeather() {
  gfx.setTransparentColor(MINI_BLACK);
  gfx.drawPalettedBitmapFromPgm(0, 55, getMeteoconIconFromProgmem(currentWeather.icon));
  // Weather Text

  gfx.setFont(ArialRoundedMTBold_14);
  gfx.setColor(MINI_BLUE);
  gfx.setTextAlignment(TEXT_ALIGN_RIGHT);
  gfx.drawString(220, 65, currentWeather.cityName);

  gfx.setFont(ArialRoundedMTBold_36);
  gfx.setColor(MINI_WHITE);
  gfx.setTextAlignment(TEXT_ALIGN_RIGHT);
   
  gfx.drawString(220, 78, String(currentWeather.temp, 1) + (IS_METRIC ? "°C" : "°F"));

  gfx.setFont(ArialRoundedMTBold_14);
  gfx.setColor(MINI_YELLOW);
  gfx.setTextAlignment(TEXT_ALIGN_RIGHT);
  gfx.drawString(220, 118, currentWeather.description);

}

void drawForecast1(MiniGrafx *display, CarouselState* state, int16_t x, int16_t y) {
  drawForecastDetail(x + 10, y + 165, 0);
  drawForecastDetail(x + 95, y + 165, 1);
  drawForecastDetail(x + 180, y + 165, 2);
}

void drawForecast2(MiniGrafx *display, CarouselState* state, int16_t x, int16_t y) {
  drawForecastDetail(x + 10, y + 165, 3);
  drawForecastDetail(x + 95, y + 165, 4);
  drawForecastDetail(x + 180, y + 165, 5);
}

void drawForecast3(MiniGrafx *display, CarouselState* state, int16_t x, int16_t y) {
  drawForecastDetail(x + 10, y + 165, 6);
  drawForecastDetail(x + 95, y + 165, 7);
  drawForecastDetail(x + 180, y + 165, 8);
}

// helper for the forecast columns
void drawForecastDetail(uint16_t x, uint16_t y, uint8_t dayIndex) {
  gfx.setColor(MINI_YELLOW);
  gfx.setFont(ArialRoundedMTBold_14);
  gfx.setTextAlignment(TEXT_ALIGN_CENTER);
  time_t time = forecasts[dayIndex].observationTime + dstOffset;
  struct tm * timeinfo = localtime (&time);
  gfx.drawString(x + 25, y - 15, WDAY_NAMES[timeinfo->tm_wday] + " " + String(timeinfo->tm_hour) + ":00");

  gfx.setColor(MINI_WHITE);
  gfx.drawString(x + 25, y, String(forecasts[dayIndex].temp, 1) + (IS_METRIC ? "°C" : "°F"));

  gfx.drawPalettedBitmapFromPgm(x, y + 15, getMiniMeteoconIconFromProgmem(forecasts[dayIndex].icon));
  gfx.setColor(MINI_BLUE);
  gfx.drawString(x + 25, y + 60, String(forecasts[dayIndex].rain, 1) + (IS_METRIC ? "mm" : "in"));
}


// draw moonphase and sunrise/set and moonrise/set
void drawInsideData() {

  const int x = 20;
  const int y = 315;

  gfx.setTextAlignment(TEXT_ALIGN_CENTER);
  gfx.setColor(MINI_YELLOW);
  gfx.drawString(x+115, y, F("Luft und Luftqualität"));
  gfx.setFont(ArialRoundedMTBold_14);

  gfx.setColor(MINI_WHITE);  
  gfx.setTextAlignment(TEXT_ALIGN_LEFT);
  gfx.drawString(x+65,  y+26, String(((int)((weather.bmeHumidity + weather.scdHumidity) / 2))) + " %");
  gfx.drawString(x+65, y+41, String(((int)((weather.scdTemperature + weather.bmeTemperature)/2))) + " °C");
  gfx.drawString(x+65, y+56, String((int)(weather.bmeIaq)) + " qty");
  gfx.drawString(x+65, y+71, String((int)(weather.bmeAccuracy)) + " Acc");
  
  gfx.drawString(x+195,  y+26, String(((int)(weather.scdCarbonDioxide))) + " ppm");
  gfx.drawString(x+195,  y+41, String(((int)(weather.bmePressure/100) + PERSONAL_ALTITUTE_PRESSURE_OFFSET)) + " mBar");
  gfx.drawString(x+195,  y+56, String(((int)(weather.bmeGasResistance/1000))) + " kOhm");

  gfx.setTextAlignment(TEXT_ALIGN_RIGHT);
  gfx.drawString(x + 60, y+26, F("Feuchte:"));
  gfx.drawString(x + 60, y+41, F("Temp.:"));
  gfx.drawString(x + 60, y+56, F("Luftquali.:"));
  gfx.drawString(x + 60, y+71, F("Accuracy:"));

  gfx.drawString(x+190,  y+26, F("CO2:"));
  gfx.drawString(x+190, y+41, F("Druck:"));
  gfx.drawString(x+190, y+56, F("Gas Res:"));

}

void drawCurrentWeatherDetail() {
  gfx.setFont(ArialRoundedMTBold_14);
  gfx.setTextAlignment(TEXT_ALIGN_CENTER);
  gfx.setColor(MINI_WHITE);
  gfx.drawString(120, 2, F("Current Conditions"));

  String degreeSign = "°C";

  drawLabelValue(0, F("Temperature:"), currentWeather.temp + degreeSign);
  drawLabelValue(1, F("Wind Speed:"), String(currentWeather.windSpeed, 1) + (IS_METRIC ? "m/s" : "mph") );
  drawLabelValue(2, F("Wind Dir:"), String(currentWeather.windDeg, 1) + "°");
  drawLabelValue(3, F("Humidity:"), String(currentWeather.humidity) + "%");
  drawLabelValue(4, F("Pressure:"), String(currentWeather.pressure) + "hPa");
  drawLabelValue(5, F("Clouds:"), String(currentWeather.clouds) + "%");
  drawLabelValue(6, F("Visibility:"), String(currentWeather.visibility) + "m");
}

void drawLabelValue(uint8_t line, String label, String value) {
  const uint8_t labelX = 15;
  const uint8_t valueX = 150;
  gfx.setTextAlignment(TEXT_ALIGN_LEFT);
  gfx.setColor(MINI_YELLOW);
  gfx.drawString(labelX, 30 + line * 15, label);
  gfx.setColor(MINI_WHITE);
  gfx.drawString(valueX, 30 + line * 15, value);
}

// converts the dBm to a range between 0 and 100%
int8_t getWifiQuality() {
  int32_t dbm = WiFi.RSSI();
  if(dbm <= -100) {
      return 0;
  } else if(dbm >= -50) {
      return 100;
  } else {
      return 2 * (dbm + 100);
  }
}

void drawWifiQuality() {
  int8_t quality = getWifiQuality();
  gfx.setColor(MINI_WHITE);
  gfx.setTextAlignment(TEXT_ALIGN_RIGHT);  
  gfx.drawString(228, 9, String(quality) + "%");
  for (int8_t i = 0; i < 4; i++) {
    for (int8_t j = 0; j < 2 * (i + 1); j++) {
      if (quality > i * 25 || j == 0) {
        gfx.setPixel(230 + 2 * i, 18 - j);
      }
    }
  }
}

void drawForecastTable(uint8_t start) {
  gfx.setFont(ArialRoundedMTBold_14);
  gfx.setTextAlignment(TEXT_ALIGN_CENTER);
  gfx.setColor(MINI_WHITE);
  gfx.drawString(120, 2, F("Forecasts"));
  uint16_t y = 0;

  String degreeSign = "°F";
  if (IS_METRIC) {
    degreeSign = "°C";
  }
  for (uint8_t i = start; i < start + 4; i++) {
    gfx.setTextAlignment(TEXT_ALIGN_LEFT);
    y = 45 + (i - start) * 75;
    if (y > SCREEN_HEIGHT) {
      break;
    }
    gfx.setColor(MINI_WHITE);
    gfx.setTextAlignment(TEXT_ALIGN_CENTER);
    time_t time = forecasts[i].observationTime + dstOffset;
    struct tm * timeinfo = localtime (&time);
    gfx.drawString(120, y - 15, WDAY_NAMES[timeinfo->tm_wday] + " " + String(timeinfo->tm_hour) + ":00");

   
    gfx.drawPalettedBitmapFromPgm(0, 15 + y, getMiniMeteoconIconFromProgmem(forecasts[i].icon));
    gfx.setTextAlignment(TEXT_ALIGN_LEFT);
    gfx.setColor(MINI_YELLOW);
    gfx.setFont(ArialRoundedMTBold_14);
    gfx.drawString(10, y, forecasts[i].main);
    gfx.setTextAlignment(TEXT_ALIGN_LEFT);
    
    gfx.setColor(MINI_BLUE);
    gfx.drawString(50, y, "T:");
    gfx.setColor(MINI_WHITE);
    gfx.drawString(70, y, String(forecasts[i].temp, 0) + degreeSign);
    
    gfx.setColor(MINI_BLUE);
    gfx.drawString(50, y + 15, "H:");
    gfx.setColor(MINI_WHITE);
    gfx.drawString(70, y + 15, String(forecasts[i].humidity) + "%");

    gfx.setColor(MINI_BLUE);
    gfx.drawString(50, y + 30, "P: ");
    gfx.setColor(MINI_WHITE);
    gfx.drawString(70, y + 30, String(forecasts[i].rain, 2) + (IS_METRIC ? "mm" : "in"));

    gfx.setColor(MINI_BLUE);
    gfx.drawString(130, y, "Pr:");
    gfx.setColor(MINI_WHITE);
    gfx.drawString(170, y, String(forecasts[i].pressure, 0) + "hPa");
    
    gfx.setColor(MINI_BLUE);
    gfx.drawString(130, y + 15, "WSp:");
    gfx.setColor(MINI_WHITE);
    gfx.drawString(170, y + 15, String(forecasts[i].windSpeed, 0) + (IS_METRIC ? "m/s" : "mph") );

    gfx.setColor(MINI_BLUE);
    gfx.drawString(130, y + 30, "WDi: ");
    gfx.setColor(MINI_WHITE);
    gfx.drawString(170, y + 30, String(forecasts[i].windDeg, 0) + "°");

  }
}
extern "C" int rom_phy_get_vdd33();
float voltage = ((float)rom_phy_get_vdd33()) / 1000;
void drawAbout() {
  gfx.fillBuffer(MINI_BLACK);

  gfx.setFont(ArialRoundedMTBold_14);
  gfx.setTextAlignment(TEXT_ALIGN_CENTER);
  drawLabelValue(7, F("Heap Mem:"), String(ESP.getFreeHeap() / 1024)+"kb");
  drawLabelValue(8, F("Flash Mem:"), String(ESP.getFlashChipSize() / 1024 / 1024) + "MB");
  drawLabelValue(9, F("WiFi Strength:"), String(WiFi.RSSI()) + "dB");
  drawLabelValue(10, F("Chip ID:"), String(ESP.getChipRevision()));
  drawLabelValue(11, F("VCC: "), String(voltage / 1024.0) +"V");
  drawLabelValue(12, F("CPU Freq.: "), String(ESP.getCpuFreqMHz()) + "MHz");
  char time_str[15];
  const uint32_t millis_in_day = 1000 * 60 * 60 * 24;
  const uint32_t millis_in_hour = 1000 * 60 * 60;
  const uint32_t millis_in_minute = 1000 * 60;
  uint8_t days = millis() / (millis_in_day);
  uint8_t hours = (millis() - (days * millis_in_day)) / millis_in_hour;
  uint8_t minutes = (millis() - (days * millis_in_day) - (hours * millis_in_hour)) / millis_in_minute;
  sprintf(time_str, "%2dd%2dh%2dm", days, hours, minutes);
  drawLabelValue(13, "Uptime: ", time_str);
}

void calibrationCallback(int16_t x, int16_t y) {
  gfx.setColor(1);
  gfx.fillCircle(x, y, 10);
}


bool _isTouchActive = false ;
int lastTouchRun = 0;

void HandleTouch() {
  if ((millis() - lastTouchRun)>100) {
    //Serial.println(String(F("HandleTouch:")) + String(millis() - lastTouchRun)); 
    lastTouchRun = millis();
    if (touchController.isTouched(0)) {
      TS_Point p = touchController.getPoint();
      if (isNewTouch()) {
        beginTouch();
        onTouch(p);
      } else {
        onTouchHold(p);
      }
    } else {
      if (isTouchActive()) {
        endTouch();
      }
    }
  }
}

bool isNewTouch() {
  return !_isTouchActive;
}

bool isTouchActive() {
  return _isTouchActive;
}

void beginTouch() {  
  Serial.println(F("Begin touch: ")); 
  _isTouchActive = true;
  touchLengthTimer = millis();
}

void endTouch() {
  Serial.println(F("End touch: ")); 
  _isTouchActive = false;
}


  
/**The MIT License (MIT)
Copyright (c) 2018 by Daniel Eichhorn
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
See more at https://blog.squix.org
*/
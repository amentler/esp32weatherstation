#pragma once
#ifndef SENSORS_h
#define SENSORS_h

#include <Arduino.h>
#define DEBUG

typedef struct Weather {
    float scdCarbonDioxide;
    float scdTemperature;
    float scdHumidity;
    float bmePressure;
    float bmeGasResistance;
    float bmeTemperature;
    float bmeHumidity;
    float bmeIaq;
    int bmeAccuracy;
} Weather;
#define SEALEVELPRESSURE_HPA (1013.25)

void Debug(String);
void sensorSetup();

void getCurrentSensorData(Weather* weather);
#endif
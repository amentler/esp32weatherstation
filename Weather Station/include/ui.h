#pragma once
#ifndef ui_h
#define ui_h

#include "ArialRounded.h"
#include <Arduino.h>
#include <ILI9341_SPI.h>
#include <MiniGrafx.h>
#include <OpenWeatherMapCurrent.h>
#include <Astronomy.h>
#include <MiniGrafx.h>



#define MINI_BLACK 0
#define MINI_WHITE 1
#define MINI_YELLOW 2
#define MINI_BLUE 3


// defines the colors usable in the paletted 16 color frame buffer
extern uint16_t palette[];

extern void drawAstronomy(MiniGrafx gfx, 
String moonAgeImage,uint8_t moonAge, time_t dstOffset,
OpenWeatherMapCurrentData currentWeather,
Astronomy::MoonData moonData,String moonPhase);


extern String getTime(time_t *timestamp);

#endif
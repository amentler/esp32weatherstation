#pragma once
#ifndef BLINGBLING_H
#define BLINGBLING_H

#include "sensors.h"
void setupBling();

void StartBling(unsigned long, Weather);
void EndBling();
void UpdateBling(unsigned long, Weather);
bool isBlingActive();
#endif
#pragma once
#ifndef EASING_H
#define EASING_H

/*projection from a sensor value to a color value.
*/
int calculateColorIntensity(int sensorValue, int normalValue, int normalColor, int minValue, int minColor, int maxValue, int maxColor);
#endif
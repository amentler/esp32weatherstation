#pragma once
#ifndef ANIMATIONS_H
#define ANIMATIONS_H

#include <NeoPixelBus.h>
#include "sensors.h"

enum BlingState {NOTHING, ANIMBUILD, ANIMKITT, BUILDUP, PULSEUP, KITTCLOCK};

extern RgbColor black;
extern RgbColor white;
extern RgbColor lightGrey;
extern RgbColor grey;
extern RgbColor darkGrey;

extern RgbColor ringColors[];

void updateAnimationColors(unsigned long time, Weather weather);

bool animBuild(unsigned long time, Weather weather, int counter) ;
bool animKitt(unsigned long time, Weather weather, int counter) ;
bool buildUp(unsigned long time, Weather weather, int counter) ;
bool pulseUp(unsigned long time, Weather weather, int counter);
bool kittClock(unsigned long time, Weather weather, int counter);

typedef bool (*StateFunction) (unsigned long time, Weather weather, int frame);

#endif